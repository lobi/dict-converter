class DictConverter():

  """ Class to convert keys of a dictionary based on a mapping """
  
  def __init__(self, mapping):
    """Initialize new converter object with the specified mapping"""
    self._mapping = mapping
    self._backward_mapping = DictConverter._revert_map(mapping)

  @staticmethod
  def _convert(input_dict, mapping):
    converted_dict = dict()
    for in_key in input_dict.keys():
      for map_key in mapping:
        if map_key == in_key:
          converted_dict[mapping[in_key]] = input_dict[in_key]
          break
      else:
        converted_dict[in_key] = input_dict[in_key]
    return converted_dict

  @staticmethod
  def _revert_map(mapping):
    res = dict((v,k) for k,v in mapping.items())
    return res

  def convertForward(self, input_dict):
    """retruns a dict with translated keys according to the specified mapping"""
    return DictConverter._convert(input_dict, self._mapping)

  def convertBackward(self, input_dict):
    """returns a dict with translated keys according to mapping values"""
    return DictConverter._convert(input_dict, self._backward_mapping)
