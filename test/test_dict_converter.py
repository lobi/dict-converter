import unittest
from .. import dict_converter

class TestDictConverter(unittest.TestCase):

  def setUp(self):
    self.converter = dict_converter.DictConverter(
      mapping = {
        b'username'.decode(): b'uid'.decode()})
    self.from_dict = {
      'username': 'dude',
      'givenName': 'Bob',
      'surName': 'Dude'}
    self.to_dict = {
      'uid': 'dude',
      'givenName': 'Bob',
      'surName': 'Dude'}

  def testInit(self):
    self.assertIsInstance(self.converter, dict_converter.DictConverter)
    self.assertIn(member = 'convertForward', container = dir(self.converter))
    self.assertIn(member = 'convertBackward', container = dir(self.converter))

  def testConvertForward(self):
    converted_dict = self.converter.convertForward(input_dict = self.from_dict)
    self.assertNotEqual(self.from_dict, self.to_dict)
    self.assertEqual(converted_dict, self.to_dict)

  def testConvertBackward(self):
    converted_dict = self.converter.convertBackward(input_dict = self.to_dict)
    self.assertNotEqual(self.from_dict, self.to_dict)
    self.assertEqual(converted_dict, self.from_dict)